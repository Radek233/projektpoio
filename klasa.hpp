#include <iostream>
using namespace std;

class klasa
{
private:
    int krotszy_bok;
    int dluzszy_bok;

public:

    Prostokat(); // konstruktor bez parametrów
    Prostokat(int krotszy_bok, int dluzszy_bok)// konstruktor z dwiema zmiennymi
    {
        this->krotszy_bok = krotszy_bok;
        this->dluzszy_bok = dluzszy_bok;
    }


    void Obwod()
    {
        cout << "Obwod prostokata :" << 2 * krotszy_bok + 2 * dluzszy_bok << endl;
    }

    void Pole()
    {
        cout << "Pole prostokata :" << krotszy_bok * dluzszy_bok << endl;
    }
};
int main()
{
    Prostokat oblicz(2, 11);
    oblicz.Obwod();
    oblicz.Pole();
}
