//============================================================================
// Name        : BankAccount.h
// Author      : L.M.
// Version     : 1.0.0
// Copyright   : Your copyright notice
// Description : The simple class modeling bank account, SUT for GoogleTest
//============================================================================

#include <iostream>
#include <sstream>
#include <vector>

class BankAccount {
	std::string id;
	std::string owner;
	double balance;
	double minBalance;
	std::vector<std::string> history;
public:
	// empty account
	BankAccount() :
			id(""), owner(""), balance(0.0), minBalance(10.) {
	}

  // initialized account
	BankAccount(std::string id, std::string owner, double balance,
			double minBalance) :
			id(id), owner(owner), balance(balance), minBalance(minBalance) {
		log("Account Initialized", 0.0, "System");
	}

	void deposit(double amount, std::string from) {
		balance += amount;
		log("deposit()", amount, from);
	}

	void withdraw(double amount, std::string to) {
		if (balance - amount >= 100.) { // BUG, initial code, before minBalance
//		if (balance - amount >= minBalance) {
			balance -= amount;
			log("withdraw()", amount, to);
		} else {
			log("withdraw() FAILED", amount, to);
		}
	}

	double getBalance() {
		return balance;
	}

	void log(std::string operation, double amount, std::string who) {
		std::string r = operation + who;
		history.push_back(r);
	}

	const std::string& getHistory(unsigned int pos) {
		if (pos >= 0 && pos < history.size())
			return history[pos];
		else
			throw pos;
	}

	unsigned int getHistorySize() {
		return history.size();
	}

	const std::string& getId() const {
		return id;
	}

	const std::string& getOwner() const {
		return owner;
	}

	double getMinBalance() const {
		return minBalance;
	}
};

