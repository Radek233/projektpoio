//============================================================================
// Name        : GoogleTest.cpp
// Author      : L.M.
// Version     : 1.0.0
// Copyright   : Your copyright notice
// Description : The initial GoogleTest example
//============================================================================

#include <iostream>
#include "../googletest/include/gtest/gtest.h"

#include "BankAccount.h"

TEST(BankAccountTest, ZeroInitialized) {
	BankAccount ba;
	EXPECT_EQ("", ba.getId());
	EXPECT_EQ("", ba.getOwner());
	EXPECT_EQ(0.0, ba.getBalance());
	EXPECT_EQ(0, ba.getHistorySize());
}

TEST(BankAccountTest, FullyInitialized) {
	std::string id = "id1", owner = "owner1";
	double balance = 0.0, minBalance = 10.;
	BankAccount ba(id, owner, balance, minBalance);
	EXPECT_EQ(id, ba.getId());
	EXPECT_EQ(owner, ba.getOwner());
	EXPECT_EQ(balance, ba.getBalance());
	EXPECT_EQ(1, ba.getHistorySize());
}

TEST(BankAccountTest, Deposit) {
	BankAccount ba;
	double add = 1000.0;
	ba.deposit(add, "L.M.");
	EXPECT_EQ(add, ba.getBalance());
	EXPECT_EQ(1, ba.getHistorySize());
}

TEST(BankAccountTest, Withdraw) {
	BankAccount ba;
	double add = 1000.;
	ba.deposit(add, "L.M.");
	EXPECT_EQ(add, ba.getBalance());
	ba.withdraw(add / 2., "L.M.");
	EXPECT_EQ(add / 2., ba.getBalance());
	ba.withdraw(add / 2. - ba.getMinBalance(), "L.M.");
	EXPECT_EQ(ba.getMinBalance(), ba.getBalance()); // now we are nearly BROKE
	ba.withdraw(ba.getBalance(), "L.M.");
	EXPECT_EQ(ba.getMinBalance(), ba.getBalance());
}

TEST(BankAccountTest, GetHistorySize) {
	BankAccount ba;
	double add = 1000.;
	ba.deposit(add, "L.M.");
	ba.deposit(add, "S.M.");
	ba.deposit(add, "M.M.");
	EXPECT_EQ(3, ba.getHistorySize());
}

TEST(BankAccountTest, GetHistory) {
	BankAccount ba;
	double add = 1000.;
	ba.deposit(add, "L.M.");
	ba.deposit(add, "S.M.");
	ba.deposit(add, "M.M.");
	// using equivalence partitioning and boundary value analysis
	EXPECT_NO_THROW(ba.getHistory(ba.getHistorySize()-2)); // eq.partitioning represent.
	EXPECT_NO_THROW(ba.getHistory(ba.getHistorySize()-1)); // boundary value
	EXPECT_THROW(ba.getHistory(ba.getHistorySize()), unsigned int); // boundary value
	EXPECT_THROW(ba.getHistory(ba.getHistorySize()+1), unsigned int); //  eq.partitioning represent.
}

int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	std::cout << "Starting tests ..."  << std::endl << std::flush;
	int res = RUN_ALL_TESTS();
	std::cout << std::endl << "RUN_ALL_TESTS(): " << res;
	return 0;
}
